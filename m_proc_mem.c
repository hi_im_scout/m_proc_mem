/* anewkirk */

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <string.h>
#include <regex.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include "m_proc_mem.h"

/*
 * Matches address range of the form 7f4c2fce1000-7f4c2fcf1000,
 * with the beginning of the range in group 1 and the end of the
 * range in group 2.
 */
#define MAP_BOUNDARY_REGEXP "^([a-f0-9]{1,})-([a-f0-9]{1,})"

FILE *open_mem(const uint32_t pid) {
  char memfile_path[30];
  FILE *fp;

  sprintf(memfile_path, "/proc/%d/mem", pid);

  if(NULL == (fp = fopen(memfile_path, "r+"))) {
    return NULL;
  }

  if(-1 == ptrace(PTRACE_ATTACH, pid, NULL, NULL)) {
    return NULL;
  }

  if(-1 == waitpid(pid, NULL, 0)) {
    return NULL;
  }

  return fp;
}

int32_t close_mem(const uint32_t pid, FILE *fp) {
  if(-1 == ptrace(PTRACE_DETACH, pid, NULL, NULL)) {
    return -1;
  }

  return fclose(fp);
}

int8_t read_chunk(FILE *fp, uint8_t *buf, uint64_t len, uint64_t offset) {
  if(0 != fseek(fp, offset, SEEK_SET)) {
    return -1;
  }

  fread(buf, 1, len, fp);

  if(0 != ferror(fp)) {
    return -1;
  }
  return 0;
}

Vector *read_maps(uint32_t pid) {
  char mapfile_path[30];
  sprintf(mapfile_path, "/proc/%d/maps", pid);

  Vector *out = calloc(1, sizeof(Vector));
  vec_init(out);
  
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  int32_t read;
  int8_t parse_ret;

  fp = fopen(mapfile_path, "r");
  if(fp == NULL) {
    return NULL;
  }
  
  while((read = getline(&line, &len, fp)) != -1) {
    MappedRegion *current = calloc(1, sizeof(MappedRegion));

    /* If the line doesn't parse or is excluded, it 
       shouldn't be included in the result */
    if(0 == parse_map_line(line, current)) {
      vec_append(out, current);
    } else {
      free(current);
    }
    
  }

  fclose(fp);
  free(line);
  return out;
}

int8_t parse_map_line(char* line, MappedRegion *out) {
  regex_t re;
  char* pattern = MAP_BOUNDARY_REGEXP;

  /* Index 1 will be the start address of the region, and
     index 2 will be the end address of the region (+1). */
  regmatch_t pmatch[3];

  if(regcomp(&re, pattern, REG_EXTENDED)) {
    return -1;
  }

  if(regexec(&re, line, 3, pmatch, 0)) {
    return -1;
  }

  /* Don't try to read/manipulate memory mapped for virtual
     system calls */
  if(strstr(line, "[vsyscall]")) {
    return -1;
  }
  
  uint16_t startRegionLen = pmatch[1].rm_eo - pmatch[1].rm_so;
  uint16_t endRegionLen = pmatch[2].rm_eo - pmatch[2].rm_so;
  char *endRegionBegin = line + pmatch[2].rm_so;
  char *startRegion = strndup(line, startRegionLen);
  char *endRegion = strndup(endRegionBegin, endRegionLen);

  out->start = strtol(startRegion, NULL, 16);
  out->end = strtol(endRegion, NULL, 16);

  free(startRegion);
  free(endRegion);
  regfree(&re);
  return 0;
}

int8_t search_region(const FILE *fp, const MappedRegion *region, const uint8_t *val, const uint32_t valLength, Vector *matches) {
  uint64_t regionLength = region->end - region->start;
  uint8_t *regionData = calloc(regionLength, 1);
  
  uint8_t readRet = read_chunk(fp, regionData, regionLength, region->start);
  if(-1 == readRet){
    return -1;
  }
  
  uint32_t matchesFound = 0;
  uint64_t filePos;
  
  for(filePos = 0; filePos <= regionLength - valLength; filePos++) {
    void *currentFilePos = region->start + filePos;
    if(!memcmp(val, regionData + filePos, valLength)) {
      void **matchPtr = calloc(sizeof(void*), 1);
      *matchPtr = currentFilePos;
      vec_append(matches, matchPtr);
      matchesFound++;
    }
  }
  
  free(regionData);
  return matchesFound;
}

int8_t search_all_regions(const FILE *fp, Vector *maps, const uint8_t *val, const uint32_t valLength, Vector *matches) {
  uint8_t ret;
  uint64_t total = 0;
  for(uint64_t i = 0; i < maps->size; i++) {
    ret = search_region(fp, vec_get(maps, i), val, valLength, matches);
    if(-1 == ret) {
      return -1;
    } else {
      total += ret;
    }
  }
  return total;
}
