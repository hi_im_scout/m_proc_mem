/* anewkirk */

#include <stdint.h>
#include "vector.h"

/* Represents a single mapped memory region in a 
   process. */
typedef struct {
  uint64_t start;
  uint64_t end;
} MappedRegion;


/* Opens memory of a process, ptraces it, and
  waits for the specified process to pause.
  Returns NULL if unsuccessful. */
FILE *open_mem(const uint32_t pid);

/* Detaches ptrace and closes the file.
   Returns 0 on success. */
int32_t close_mem(const uint32_t pid, FILE *fp);

/* Reads a region of memory of size len into buf 
   beginning at offset. Returns 0 on success. */
int8_t read_chunk(FILE *fp, uint8_t *buf, const uint64_t len, const uint64_t offset);

/* Returns a Vector of MappedRegions for all memory
   maps in /proc/$pid/maps. Returns NULL on failure.
   Allocates dynamic memory--use vec_free_all() to clean
   up afterwards. Returns NULL if unsuccessful. */
Vector *read_maps(const uint32_t pid);

/* Parses a line of a process's memory map, and
   places the results into out. Returns 0 on success. */
int8_t parse_map_line(char* line, MappedRegion *out);

/* Searches region for val and adds all matching addresses to
   matches. Returns -1 if unsuccessful, otherwise returns the 
   number of matches found. */
int8_t search_region(const FILE *fp, const MappedRegion *region, const uint8_t *val, const uint32_t valLength, Vector *matches);

/* Searches all regions in maps for val and adds all matching
   addresses to matches. Returns -1 if unsuccessful, otherwise
   returns the total number of matches found. */
int8_t search_all_regions(const FILE *fp, Vector *maps, const uint8_t *val, const uint32_t valLength, Vector *matches);

