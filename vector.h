/* anewkirk */

#include <stdint.h>
#include <stdlib.h>

typedef struct {
  uint64_t size;
  uint64_t capacity;
  void **data;
} Vector;

void vec_init(Vector *vec);

void vec_append(Vector *vec, void *val);

void *vec_get(Vector *vec, uint32_t index);

void vec_set(Vector *vec, uint32_t index, void *val);

int8_t vec_resize_if_full(Vector *vec);

void vec_free(Vector *vec);

void vec_free_all(Vector *vec);
