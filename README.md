# **m\_proc\_mem**
*A small C library for manipulating the memory space of running processes on linux and Android.*

---

## Usage
---

## Building

Just `git clone` the repo and `make`. Default build target outputs a shared library.

---
## Features

* Scan the memory of a running process for specific values

* Read chunks of memory from a running process

---

## Feature Roadmap

* Ranged search

* Allow incremental searching across match list to find changes

* Modify values

* Parallel search

* Hotpatching/code injection

* API hooking

### Stretch Goals

---

## Meta

Licensed under the [MIT License](https://opensource.org/licenses/MITL).

Alex Newkirk

https://bitbucket.org/alex_newkirk/

alex.newkirk0@gmail.com
