CC = gcc
CFLAGS = -Wall -Wextra -Wpedantic -O2 -fpic -I.

lib: m_proc_mem.h
	$(CC) -L. -Wl,-rpath=. -g -c m_proc_mem.c -o m_proc_mem.o $(CFLAGS) -lvector
	$(CC) -shared -o libmprocmem.so m_proc_mem.o;

clean:
	rm -f ./m_proc_mem.o; rm -f ./libmprocmem.so;rm -f ./test.out; 

test:
	gcc -L. -Wl,-rpath=. -g test.c -o test.out -lmprocmem -lvector
